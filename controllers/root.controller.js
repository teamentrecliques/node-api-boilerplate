const run = (req, res) => {
    res
        .status(200)
        .send({
            success: true,
            message: 'API is running!',
        });
};

module.exports = { run };
