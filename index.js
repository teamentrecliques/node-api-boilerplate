const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');

const app = express();

app.use(logger('tiny'));
app.use(bodyParser.json());

// --> Configs
const { port, mode } = require('./configs/api.config');

// --> Functions
const { normalizePort } = require('./functions/api.functions');
const { connect } = require('./functions/db.functions');

// --> Instanciate Routers
const Routers = {
    v1: express.Router(),
};

// --> Import Middlewares
const Middlewares = {
    default: require('./middlewares/default.middleware'),
};

// --> Import Routes
const Routes = {
    root: require('./routes/root.routes'),
};

Routers.v1.use('/', Middlewares.default);

// --> Apply Routes
Routers.v1.use('/', Routes.root);

// --> Apply Routers
app.use('/api/v1', Routers.v1);

app.listen(normalizePort(), () => {
    console.log(`[API] Mode: ${mode}`);
    console.log(`[API] Online on ${port}`);

    connect();
});
