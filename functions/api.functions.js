// --> Configs
const config = require('../configs/api.config');

const normalizePort = () => {
    const portNumber = config.port;
    const port = (typeof portNumber === 'string') ? parseInt(portNumber, 10) : portNumber;
    if (isNaN(port)) return portNumber;
    else if (port > 0) return port;
    return false;
};

module.exports = { normalizePort };
