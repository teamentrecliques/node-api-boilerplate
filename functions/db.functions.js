const mongoose = require('mongoose');

const { mode } = require('../configs/api.config');
const config = require('../configs/db.config');

const connect = () => {
    const uri = config[mode];

    // --> Connect and set global promises to use mongoose promises
    mongoose.connect(uri);
    global.Promise = mongoose.Promise;

    // --> Mongoose event handlers
    mongoose.connection.on('connected', () => {
        console.log(`[MONGOOSE] Trying connect to ${uri}`);
    });
    mongoose.connection.on('disconnected', () => {
        console.warn('[MONGOOSE] Disconnected from MongoDB');
    });
    mongoose.connection.on('error', (err) => {
        console.error(`[MONGOOSE] An error ocurred with MongoDB connection:\n${err}`);
    });
    mongoose.connection.on('open', () => {
        console.log('[MONGOOSE] Connected!');
    });
};

module.exports = { connect };
