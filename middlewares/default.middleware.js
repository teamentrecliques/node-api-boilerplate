// --> Dependencies
const Router = require('express').Router();

Router.use('/', (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'DELETE, GET, PATCH, POST, PUT');
    res.setHeader('Access-Control-Allow-Headers', ['Content-Type', 'Authorization', 'Credential']);
    next();
});

module.exports = Router;
